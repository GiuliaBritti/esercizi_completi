/*es2.js: crea una funzione parametrica che ritorna una stringa,
 logga il risultato sulla console*/

function sognare(sogno1 , sogno2){
    return sogno1 + " " + sogno2;
}
let sogno1 = "buona notte";
let sogno2 = "sogni d'oro";
let risultato = sognare  (sogno1 , sogno2);

console.log(risultato);
